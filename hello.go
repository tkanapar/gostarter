package main

import (
	"fmt"
	"io"
	"net"

	"gitlab.com/tkanapar/gochannels"
	"gitlab.com/tkanapar/gostringutils"
)

const address = "localhost:4000"

func helloNetwork() {
	l, err := net.Listen("tcp", address)
	if err == nil {
		for {
			fmt.Println("Listening on port 4000")
			c, err := l.Accept()
			if err == nil {
				go io.Copy(c, c)
			} else {
				break
			}
		}
	}
}

func main() {
	fmt.Printf(gostringutils.Reverse("!og, olleH"))
	//helloNetwork()
	ch := make(chan int)
	go gochannels.Fibs(ch)
	for i := 0; i < 20; i++ {
		fmt.Print(<-ch, " ")
	}
	fmt.Println()

}
